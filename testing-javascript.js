//LOGGING OUTPUT
console.log('Hello world');
console.error('This is an error');
console.warn('This is a warning');


// DATA TYPES - String, Number, Boolean, null, undefined
const nama ='geist'
//string variable const
const age = 23;
const rating = 3.5;
//number variabel const
const isCool = true;
//bolean
const x = null;
//null (empty)
const y = undefined;
let z; 
// undefined variabel let

// Concatenation
console.log('my name is ' + nama + ' and I am ' + age)
// Template literal (better)
console.log(`My name is ${nama} and I am ${age}`);

// String methods & properties
const s = 'aku pengen fulltime';
console.log(s.substring(0,8).val = s.toUpperCase());
// let val;
// Get length
val = s.length;
// Change case
val = s.toUpperCase();
val = s.toLowerCase();
// Get sub string
val = s.substring(0, 5);
// Split into array
val = s.split('');

// ARRAYS - Store multiple values in a variable
const numbers = [1,2,3,4,5];

const fruits = ['apples', 'oranges', 'pears', 'grapes'];

fruits[3] = 'grapes';
// Add value
fruits.push('mangos');
// Add value using push()
fruits.unshift('strawnerries');
// Add to beginning
fruits.pop();
// Remove last value
console.log(Array.isArray(fruits));
// // Check if array
console.log(fruits.indexOf('oranges'));
// // Get index
console.log( numbers, fruits);

// OBJECT LITERALS
const person = {
    firstName: 'geist',
    lastname: 'pangestu',
    age: 23,
    hobbies: ['music', 'movies', 'play game'],
    address: {
      street: 'Gedong Kuning',
      city: 'Yogyakarta',
      state: 'Daerah Istimewa Yogyakarta'
    }
  }

  person.email = 'gesit4511@gmail.com';
  console.log(person);

  // Array of objects
const todos = [
    {
      id: 1,
      text: 'Take out trash',
      isComplete: false
    },
    {
      id: 2,
      text: 'Dinner with wife',
      isComplete: false
    },
    {
      id: 3,
      text: 'Meeting with boss',
      isComplete: true
    }
  ];

  // Format as JSON
const todoJSON = JSON.stringify(todos);
console.log(todoJSON);

// LOOPS

// For
for(let i = 0; i <= 10; i++){
    console.log(`For Loop Number: ${i}`);
  }
  
  // While
let i = 0
while(i <= 10) {
    console.log(`While Loop Number: ${i}`);
    i++;
  }

// Loop Through Arrays
// For Loop
for(let i = 0; i < todos.length; i++){
    console.log(` Todo ${i + 1}: ${todos[i].text}`);
  }
  
  // For...of Loop
  for(let todo of todos) {
    console.log(todo.text);
  }

// map() - Loop through and create new array
  
const todoText = todos.map(function(todo) {
    return todo.text;
  });
  
  console.log(todoText);

  const todoCompleted = todos.filter(function(todo) {
    return todo.isComplete === true;
  }).map(function(todo){
      return todo.text;
  })
  
  console.log(todoCompleted);

// Simple If/Else Statement
const a = 30;

if(a === 10) {
  console.log('a is 10');
} else if(a > 10) {
  console.log('a is greater than 10');
} else {
  console.log('a is less than 10')
}

// Switch
color = 'blue';

switch(color) {
  case 'red':
    console.log('color is red');
  case 'blue':
    console.log('color is blue');
  default:  
    console.log('color is not red or blue')
}

// Ternary operator / Shorthand if
const c = color === 'red' ? 10 : 20;

//function number 
function addNums(num1 = 1, num2 = 1){
  return num1 + num2;
}

console.log(addNums(5 , 5));

const AddNums = (num1 = 1, num2 = 1) => num1+num2;

console.log(addNums(5 , 5));

// Constructor Function
function Person(firstName, lastName, dob) {
  // Set object properties
  this.firstName = firstName;
  this.lastName = lastName;
  this.dob = new Date(dob);


  // Get Birth Year
  this.getBirthYear = function () {
    return this.dob.getFullYear();
  }

  this.getFullName = function () {
    return `${this.firstName} ${this.lastName}`;
  }

}

// //class
// // class Person {
// //   constructor(firstName, lastName, dob){
// //     this.firstName = firstName;
// //     this.lastName = lastName;
// //     this.dob = new Date(dob);
  
// //   }
  
//   getBirthYear() {
//     return this.dob.getFullYear();
//   }

//   getFullName() {
//     return `${this.firstName} ${this.lastName}`;
//   }
// }

Person.prototype.getBirthYear = function() {
  return this.dob.getFullYear()
}

Person.prototype.getFullName = function() {
  return this.dob.getFullName()
}

// Instantiate an object from the class

const person1 = new Person('John', 'Doe', '7-8-80');
const person2 = new Person('Steve', 'Smith', '8-2-90');



// console.log("person 1 =",person1.getBirthYear()); 

// console.log("person 2 = ", person2.dob.getFullYear()); 

// console.log(person2.getFullName()); 

console.log(person2.getFullName());
console.log(person1);

// Single Element Selectors
console.log(document.getElementById('my-form'));
console.log(document.querySelector('.container'));
// Multiple Element Selectors
console.log(document.querySelectorAll('.item'));
console.log(document.getElementsByTagName('li'));
console.log(document.getElementsByClassName('item'));

const items = document.querySelectorAll('.item');
items.forEach((item) => console.log(item));

// MANIPULATING THE DOM
const ul = document.querySelector('.items');

// ul.remove();
// ul.lastElementChild.remove();
// ul.firstElementChild.textContent = 'Hello';
// ul.children[1].innerText = 'Brad';
// ul.lastElementChild.innerHTML = '<h1>Hello</h1>'

const btn = document.querySelector('.btn');
btn.style.background = 'red';

// Mouse Event
// btn.addEventListener('click', e => {
//   e.preventDefault();
//   console.log(e.target.className);
//   document.getElementById('my-form').style.background = '#ccc';
//   document.querySelector('body').classList.add('bg-dark');
//   ul.lastElementChild.innerHTML = '<h1>Changed</h1>';
// });

// USER FORM SCRIPT

// Put DOM elements into variables
const myForm = document.querySelector('#my-form');
const nameInput = document.querySelector('#name');
const emailInput = document.querySelector('#email');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#users');

// Listen for form submit
myForm.addEventListener('submit', onSubmit);

function onSubmit(e) {
  e.preventDefault();
  
  if(nameInput.value === '' || emailInput.value === '') {
    // alert('Please enter all fields');
    msg.classList.add('error');
    msg.innerHTML = 'Please enter all fields';

    // Remove error after 3 seconds
    setTimeout(() => msg.remove(), 3000);
  } else {
    // Create new list item with user
    const li = document.createElement('li');

    // Add text node with input values
    li.appendChild(document.createTextNode(`${nameInput.value}: ${emailInput.value}`));

    // Add HTML
    // li.innerHTML = `<strong>${nameInput.value}</strong>e: ${emailInput.value}`;

    // Append to ul
    userList.appendChild(li);

    // Clear fields
    nameInput.value = '';
    emailInput.value = '';
  }
}